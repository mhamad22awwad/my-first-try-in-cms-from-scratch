<?php 
include "include/head.php"; 
include "functions/posts.php";

$post = new Posts();

if (isset($_POST["button"])) {
  
  /**
   * post the 3 data
   */
  $titel_of_post = $_POST["titel_of_post"];
  $body_of_post  = $_POST["body_of_post"] ;
  $owner_of_post = $_POST["owner_of_post"];

  /**
   * secure the 3 data from any sql bad code
   */
  $titel_of_post = mysqli_real_escape_string($conn,$titel_of_post);
  $body_of_post = mysqli_real_escape_string($conn,$body_of_post);
  $owner_of_post = mysqli_real_escape_string($conn,$owner_of_post);

  $post->addPost($titel_of_post , $body_of_post , $owner_of_post );


}


theHead( "blog-2" , "Blog Home 2" );

?>

<?php include "include/nav.php"; ?>


  <!-- Page Content -->
  <div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">Blog Home Two
      <small>Subheading</small>
    </h1>

    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="index.php">Home</a>
      </li>
      <li class="breadcrumb-item active">Blog Home 2 <span class="ml-5"> <?php  $post->cuntPosts();  ?> </span></li>
    </ol>
    <!--           -->

    <div class="row">
      
      
      
    <div class="col-md-8">
        
      
        <!-- Add Post -->
        <div class="card mb-4">
          <div class="card-body">
            <div class="row">
              
              <div class="col-lg-12">
                <h2 class="card-title">Add POST</h2>
                <form accept-charset="UTF-8">

                  <div class="form-group" >
                    <label for="titel_of_post">Titel Of The Post <small>titel length 5 and more</small></label>
                    <input type="text" name="titel_of_post" class="form-control form-input" id="titel_of_post" placeholder="Titel Of The Post">
                  </div>
                    
                  <div class="form-group">
                    <label for="body_of_post">The Post <small>body length 20 and more</small></label>
                    <textarea type="text" name="body_of_post" class="form-control form-input" id="body_of_post" placeholder="The Post"> </textarea> 
                  </div>
                    
                  <div class="form-group">
                    <label for="owner_of_post">Post Writer <small>the owner of post name length 3 and more</small></label>
                    <input type="text" name="owner_of_post" class="form-control form-input" id="owner_of_post" placeholder="Post Writer">
                  </div>

                  <!-- <input type="button" value="doon"> -->
                  <input type="button" name="button" value="Add Post" class="btn btn-primary" id="button">
                  
                </form>
              </div>

            </div>
          </div>
          <div class="card-footer text-muted">
            <a href="#"></a>
          </div>
        </div>



        <?php echo $post->showPost(2); ?>

        <!-- Blog Post -->
        <div class="card mb-4">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6">
                <a href="#">
                  <img class="img-fluid rounded" src="http://placehold.it/750x300" alt="">
                </a>
              </div>
              <div class="col-lg-6">
                <h2 class="card-title">Post Title</h2>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                <a href="#" class="btn btn-primary">Read More &rarr;</a>
              </div>
            </div>
          </div>
          <div class="card-footer text-muted">
            Posted on January 1, 2017 by
            <a href="#">Start Bootstrap</a>
          </div>
        </div>

        <!-- Blog Post -->
        <div class="card mb-4">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6">
                <a href="#">
                  <img class="img-fluid rounded" src="http://placehold.it/750x300" alt="">
                </a>
              </div>
              <div class="col-lg-6">
                <h2 class="card-title">Post Title</h2>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                <a href="#" class="btn btn-primary">Read More &rarr;</a>
              </div>
            </div>
          </div>
          <div class="card-footer text-muted">
            Posted on January 1, 2017 by
            <a href="#">Start Bootstrap</a>
          </div>
        </div>


      </div>


      <!-- Sidebar Widgets Column -->
      <div class="col-md-4">

        <!-- Search Widget -->
        <div class="card mb-4">
          <h5 class="card-header">Search</h5>
          <div class="card-body">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search for...">
              <span class="input-group-btn">
                <button class="btn btn-secondary" type="button">Go!</button>
              </span>
            </div>
          </div>
        </div>

        <!-- Categories Widget -->
        <div class="card my-4">
          <h5 class="card-header">Categories</h5>
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                  <li>
                    <a href="#">Web Design</a>
                  </li>
                  <li>
                    <a href="#">HTML</a>
                  </li>
                  <li>
                    <a href="#">Freebies</a>
                  </li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                  <li>
                    <a href="#">JavaScript</a>
                  </li>
                  <li>
                    <a href="#">CSS</a>
                  </li>
                  <li>
                    <a href="#">Tutorials</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <!-- Side Widget -->
        <div class="card my-4">
          <h5 class="card-header">Side Widget</h5>
          <div class="card-body">
            You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 card containers!
          </div>
        </div>

      </div>

    </div>

    <!-- Pagination -->
    <ul class="pagination justify-content-center mb-4">
      <li class="page-item">
        <a class="page-link" href="#">&larr; Older</a>
      </li>
      <li class="page-item disabled">
        <a class="page-link" href="#">Newer &rarr;</a>
      </li>
    </ul>


  </div>
  <!-- /.container -->

  
<!-- Footer -->
<?php include "include/footer.php"; ?>




<script>

  $(document).ready(function(){


    $("#button").click(function(){

      if ($.trim($("#titel_of_post").val()).length >= 5) {
        var titel_of_post = $("#titel_of_post").val();
      } else {
        var titel_of_post = "";
        alert("shold enter a titel for the post");
      }

      if ($.trim($("#body_of_post").val()).length >= 20) {
        var body_of_post = $("#body_of_post").val();
      } else {
        var body_of_post = "";
        alert("shold enter a body for the post");
      }

      if ($.trim($("#owner_of_post").val()).length >= 3) {
        var owner_of_post = $("#owner_of_post").val();
      } else {
        var owner_of_post = "";
        alert("you shold enter your name or a nick name")
      }

      if (titel_of_post == "" && body_of_post == "" && owner_of_post == "") {
        alert("there is some data are have not enterd");
      } else {
        alert("all good");

        $.ajax({
          type: "POST",
          url: "blog-home-1.php",
          data: {
            button:"",
            titel_of_post : titel_of_post,
            body_of_post : body_of_post,
            owner_of_post : owner_of_post
          },
          success:function(data){
            alert("the post have added in a good way");
          }
        });
      }

    })

  })



</script>