-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 08, 2020 at 07:47 PM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 7.1.33-9+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `full_page`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) NOT NULL,
  `titel` text NOT NULL,
  `body` longtext NOT NULL,
  `writer` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `titel`, `body`, `writer`, `date`) VALUES
(1, 'ghfjjhmgh', ' dfbsndhgmfj,hgndjycnnc', 'hnghmm', '2019-08-20 23:33:56'),
(2, 'the oxejen', ' the oxijen is all every whear we need it', 'mhamad', '2019-08-20 23:33:56'),
(3, 'the ssfb', ' kjfdhjajndfj;fdn;jlkbnb;jgnfog', 'vkvjbfngbl', '2019-08-20 23:33:56'),
(4, '16hfg53', 'g+62dfg0+fgh5h+gghdyt95f232g ', 'saghm', '2019-08-20 23:33:56'),
(6, 'the post', 'the asdbbfgbfnbbfbgn\nsdsfvbgcnh\nfgd ', 'dfgdfghf', '2019-08-20 23:34:37'),
(7, 'thdsfgd', '', 'bghdt', '2019-08-21 00:31:33'),
(8, 'gfhfgj', ' fhsydtjuiruyufdsty6ekfhj', 'tyut', '2019-08-22 17:13:53');

-- --------------------------------------------------------

--
-- Table structure for table `projects_name`
--

CREATE TABLE `projects_name` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` longtext NOT NULL,
  `link` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects_name`
--

INSERT INTO `projects_name` (`id`, `name`, `description`, `link`) VALUES
(1, 'big shop', ' Ù…ØªØ¬Ø± Ø§Ù„ÙƒØªØ±ÙˆÙ†ÙŠ ÙŠØ­ØªÙˆÙŠ Ø¹Ù„Ù‰ 3 Ø§Ù‚Ø³Ø§Ù… (ÙƒÙ…Ø¨ÙŠÙˆØªØ±Ø§Øª Ùˆ Ù‡ÙˆØ§ØªÙ Ùˆ Ø³ÙŠØ§Ø±Ø§Øª) Ùˆ ÙŠØªÙ… Ø¹Ø±Ø¶Ù‡Ù… Ø¬Ù…ÙŠØ¹Ø§ ÙÙŠ Ù‚Ø³Ù… ÙˆØ§Ø­Ø¯ Ùˆ ÙŠÙ…ÙƒÙ† Ø¹Ø±Ø¶ ÙƒÙ„ Ù‚Ø³Ù… Ù„ÙˆØ­Ø¯Ù‡', 'https://bigshoping.000webhostapp.com'),
(2, 'big shop 2', 'no des', '#'),
(3, 'try 3', 'any dis ', '#'),
(4, 'try 4', 'any dis ', '#'),
(5, 'try 5', 'any dis ', '#'),
(6, 'try 6', 'any dis ', '#'),
(7, 'try 7', 'any dis ', '#'),
(17, 'try 8', 'any dis ', '#');

-- --------------------------------------------------------

--
-- Table structure for table `project_and_language`
--

CREATE TABLE `project_and_language` (
  `id` int(11) NOT NULL,
  `project` int(11) NOT NULL,
  `language` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `used_language`
--

CREATE TABLE `used_language` (
  `id` int(5) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `used_language`
--

INSERT INTO `used_language` (`id`, `name`) VALUES
(1, 'html'),
(2, 'css'),
(3, 'js'),
(4, 'bootstrap'),
(5, 'jQuery'),
(6, 'Ajax'),
(7, 'php'),
(8, 'mySQL');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects_name`
--
ALTER TABLE `projects_name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `used_language`
--
ALTER TABLE `used_language`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `projects_name`
--
ALTER TABLE `projects_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `used_language`
--
ALTER TABLE `used_language`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
