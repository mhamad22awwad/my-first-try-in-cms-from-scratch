<?php

/**
 * the header for all pages
 * and it get 2 var
 * 1- is the class of body 
 * 2- is the titel of head
 * 
 */

function theHead($page , $titel) {

    $head = "
                <!DOCTYPE html>
                <html lang='en'>
                
                <head>
                
                <meta charset='utf-8'>
                <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
                <meta http-equiv='X-UA-Compatible' content='IE=edge'>
                <meta name='description' content=''>
                <meta name='author' content=''>
                
                <title> " . $titel . " </title>
                
                <!-- Bootstrap core CSS -->
                <link href='vendor/bootstrap/css/bootstrap.min.css' rel='stylesheet'>
                
                <!-- Custom styles for this template -->
                <link href='css/modern-business.css' rel='stylesheet'>
                
                </head>
                
                <body class='". $page ."'>
            " ;

    echo $head;

}



?>