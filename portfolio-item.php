<?php 
include "include/head.php"; 
include "functions/projects.php";

$id = $_GET["id"];

$project = new SqlCode();
$data = $project->showOneProject($id);

theHead( "portfolio-item" , $data["name"] );

?>

<?php include "include/nav.php"; ?>


  <!-- Page Content -->
  <div class="container">

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">Portfolio Item For
      <small><?php echo $data["name"]; ?></small>
    </h1>

    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="index.php">Home</a>
      </li>
      <li class="breadcrumb-item active"><?php echo $data["name"]; ?></li>
    </ol>

    <!-- Portfolio Item Row -->
    <div class="row">

      <div class="col-md-8">
        <img class="img-fluid" src="http://placehold.it/750x500" title="This is <?php echo $data["name"]; ?>"  alt="This is <?php echo $data["name"]; ?>">
      </div>

      <div class="col-md-4">
        <h3 class="my-3">Project Description</h3>
        <p><?php echo $data["description"]; ?></p>
        <h3 class="my-3">Project Details</h3>
        <ul>
          <li>Lorem Ipsum</li>
          <li>Dolor Sit Amet</li>
          <li>Consectetur</li>
          <li>Adipiscing Elit</li>
        </ul>
      </div>


    </div>
    <!-- /.row -->

    <!-- Related Projects Row -->
    <h3 class="my-4">Next Projects</h3>

    <div class="row">


      <?php $project->nextProjects($id); ?>


    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  
<!-- Footer -->
<?php include "include/footer.php"; ?>