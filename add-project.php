<?php 
include "include/head.php"; 
// include "functions/conn.php";
include "functions/projects.php";

if(isset($_POST["button"])) {

  $add = new SqlCode();

  $name_of_project = $_POST["name_of_project"];
  $description_of_project = $_POST["description_of_project"];
  $link_of_project = "#";

  $add->addProject($name_of_project , $description_of_project , $link_of_project);

}

?>


<?php 
theHead( "add-project" , "Add Project" );
include "include/nav.php"; 
?>


<!-- Page Content -->
<div class="container mb-5">

  <!-- Page Heading/Breadcrumbs -->
  <h1 class="mt-4 mb-3">Add Project
    <small>Subheading</small>
  </h1>

  <ol class="breadcrumb">
    <li class="breadcrumb-item">
      <a href="index.php">Home</a>
    </li>
    <li class="breadcrumb-item active">Add Project</li>
  </ol>

  <p>  </p>

  <div>
    
    <form accept-charset="UTF-8">

      <div class="form-group" >
        <label for="name_of_project">The Name Of Project</label>
        <input type="text" name="name_of_project" class="form-control form-input" id="name_of_project" placeholder="Name of Project">
      </div>
        
      <div class="form-group">
        <label for="description_of_project">The Description Of Project</label>
        <textarea type="text" name="description_of_project" class="form-control form-input" id="description_of_project" placeholder="Description of Project"> </textarea> 
      </div>
        
      <div class="form-group">
        <label for="link_of_project">Link Of Project</label>
        <input type="text" name="link_of_project" class="form-control form-input" id="link_of_project" placeholder="Link Of Project it will add # only">
      </div>

      <!-- <input type="button" value="doon"> -->
      <input type="button" name="button" value="Add Project" class="btn btn-primary" id="button">
      
    </form>

  </div>


</div>
<!-- /.container -->

  
<!-- Footer -->
<?php include "include/footer.php"; ?>

<script>

  $(document).ready(function(){

    $("#button").click(function(){


      if($.trim($("#name_of_project").val()).length >= 1){

        var name_of_project = $("#name_of_project").val();

      } else {
        
        var name_of_project = "";
      
      }


      if ($.trim($("#description_of_project").val()).length >= 1) {
        
        var description_of_project = $("#description_of_project").val();

      } else {
        
        var description_of_project = "";
      
      }


      if ($.trim($("#link_of_project").val()).length >= 1) {

        var link_of_project = $("#link_of_project").val();
        
      } else {
        
        var link_of_project = "";

      }


      if (name_of_project != "" && description_of_project != "" && link_of_project != "" ) {
        
        alert("all done");

        $.ajax({

                type: "POST",
                url: "add-project.php",
                data: {
                  button : "",
                  name_of_project : name_of_project ,
                  description_of_project : description_of_project ,
                  link_of_project : link_of_project
                },
                success: function(data){
                  
                  alert("the project have added");

                  // $(".form-input").val("");

                }

        });


      } else {

        alert("miss some data");
        
      }


    })




  })

</script>