<?php include "conn.php"; ?>

<?php 

class Posts {
    

    /**
     * add post to the data base
     */
    public function addPost($titel , $body , $writer) {
        
        global $conn;

        // $date = date("Y-m-d H:i:s");

        $sql = "INSERT INTO posts (titel , body , writer) VALUES ('$titel' , '$body' , '$writer') ";

        if ($conn->multi_query($sql) == TRUE) {
            echo "the new post is added";
        } else {
            echo "error : ".$sql."<br>".$conn->error;
        }
        
        
    }


    /**
     * show all post in the data base
     */
    public function showPost($page) {
        global $conn;
        $sql = "SELECT * FROM posts ORDER BY id DESC";
        $result = $conn->query($sql);
        $posts = "";

        if ($result->num_rows > 0) {

            if ($page == 1) {
                
                while ($row = $result->fetch_assoc()) {
                    $posts .= "
                            <!-- Blog Post -->
    
                            <div class='card mb-4'>
                                <img class='card-img-top' src='http://placehold.it/750x300' alt='Card image cap'>
                                <div class='card-body'>
                                    <h2 class='card-title'>" . $row["titel"] . "</h2>
                                    <p class='card-text'>  " . $row["body"] . " </p>
                                    <a href='blog-post.php?id=" . $row["id"] . "' class='btn btn-primary'>Read More &rarr;</a>
                                </div>
                                <div class='card-footer text-muted'>
                                    " . $row["date"] . " by
                                    <a href='#'> " . $row["writer"] . " </a>
                                </div>
                            </div>
                            ";
                }
                
            } elseif ($page == 2 ) {
                
                while ($row=$result->fetch_assoc()) {
                    $posts .= "
                            <!-- Blog Post -->
                            <div class='card mb-4'>
                            <div class='card-body'>
                                <div class='row'>
                                <div class='col-lg-6'>
                                    <a href='#'>
                                    <img class='img-fluid rounded' src='http://placehold.it/750x300' alt=''>
                                    </a>
                                </div>
                                <div class='col-lg-6'>
                                    <h2 class='card-title'>" . $row["titel"] . "</h2>
                                    <p class='card-text'>  " . $row["body"] . " </p>
                                    <a href='blog-post.php?id=" . $row["id"] . "' class='btn btn-primary'>Read More &rarr;</a>
                                </div>
                                </div>
                            </div>
                            <div class='card-footer text-muted'>
                                    " . $row["date"] . " by
                                    <a href='#'> " . $row["writer"] . " </a>
                                </div>
                            </div>

                            ";
                }

            } else {
                echo "Sorry ";
            }

        } else {
            $posts = "Sorry there is no Posts";
        }

        
        
        return $posts;
    }


    /**
     * show the count of the posts 
     */
    public function cuntPosts() {
        global $conn;
        $sql = "SELECT * FROM posts ";

        $result = $conn->query($sql);
        $num_rows = mysqli_num_rows($result);

        echo "there is " . $num_rows . " post ";
    }


    /**
     * show the post you click
     */
    public function showOnePost($id){
        global $conn;
        $sql = "SELECT * FROM posts WHERE id= '$id'";

        $result = $conn->query($sql);

        if ($result->num_rows == 1 ) {
            $row=$result->fetch_assoc();

            
            return $row;
        } else {
            echo "
                <script>
                    window.location = '404.php'
                </script>";
        }
        

    }



}







?>