<?php include "conn.php"; ?>

<?php


class SqlCode {

    /**
     * whin thes function caled 
     * it function will get 3 var 
     * and it will add them to the db 
     */
    public function addProject($name_of_project , $description_of_project , $link_of_project ) {
        global $conn;
        $sql = "INSERT INTO projects_name (name , description , link ) VALUES ('$name_of_project' , '$description_of_project' , '$link_of_project') ";
        
        if ($conn -> multi_query($sql)==TRUE) {
			echo "data Inserted"."<br>";
		} else {
			echo "error : ".$sql."<br>".$conn->error;
		}
    }
    

    /**
     * this function will show all project in data base
     * it will display them in rows as the page disine for
     * it will show them in 1 or 2 or 3 or 4 projects in line
     */
    public function showProject($page){

        global $conn;
        $sql = "SELECT * FROM projects_name ";
        $result = $conn->query($sql);

        
        if ($result->num_rows>0) {
            
            $projectCard = "";
            
            /**
             * 
             * it will see in page 
             * 
             */
            if ($page == 1) {
                /**
                 * it will print print evry project in a singel line
                 */
                while ($row = $result->fetch_assoc()){

                    $projectCard .= "
                                        <div class='row'>
                                            <div class='col-md-7'>
                                                <a href='portfolio-item.php?id=".$row['id']."'>
                                                    <img class='img-fluid rounded mb-3 mb-md-0' src='http://placehold.it/700x300' alt=''>
                                                </a>
                                            </div>
                                            <div class='col-md-5'>
                                                <h3> " . $row["name"] . " </h3>
                                                <p> " . $row["description"] . " .</p>
                                                <a class='btn btn-primary' href='portfolio-item.php?id=".$row['id']."'>More Info 
                                                    <span class='glyphicon glyphicon-chevron-right'></span>
                                                </a>
                                                <a class='btn btn-primary' href='portfolio-item.php?id=".$row['id']."'>Go To Project 
                                                    <span class='glyphicon glyphicon-chevron-right'></span>
                                                </a>
                                            </div>
                                        </div>
                                        
                                        <hr>
                                    ";
                }
                
            }elseif ($page == 2) {
                /**
                 * it will print 2 project in one line
                 */
                while ($row = $result->fetch_assoc()){

                    $projectCard .= "
                                    <div class='col-lg-6 portfolio-item'>
                                        <div class='card h-100'>
                                            <a href='portfolio-item.php?id=".$row['id']."'>
                                            <img class='card-img-top' src='http://placehold.it/700x400' alt=''></a>
                                            <div class='card-body'>
                                                <h4 class='card-title'>
                                                    <a href='#'> " . $row["name"] . " </a>
                                                </h4>
                                                <p class='card-text'>" . $row["description"] . " .</p>
                                                <a class='btn btn-primary' href='portfolio-item.php?id=".$row['id']."'>More Info 
                                                    <span class='glyphicon glyphicon-chevron-right'></span>
                                                </a>
                                                <a class='btn btn-primary' href='portfolio-item.php?id=".$row['id']."'>Go To Project 
                                                    <span class='glyphicon glyphicon-chevron-right'></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    ";
                }

            }
            elseif ($page == 3) {
                /**
                 * it will print 3 project in one line
                 */
                while ($row = $result->fetch_assoc()){

                    $projectCard .= "
                                    <div class='col-lg-4 col-sm-6 portfolio-item'>
                                        <div class='card h-100'>
                                            <a href='portfolio-item.php?id=".$row['id']."'>
                                            <img class='card-img-top' src='http://placehold.it/700x400' alt=''></a>
                                            <div class='card-body'>
                                                <h4 class='card-title'>
                                                    <a href='#'> " . $row["name"] . " </a>
                                                </h4>
                                                <p class='card-text'>" . $row["description"] . " .</p>
                                                <a class='btn btn-primary' href='portfolio-item.php?id=".$row['id']."'>More Info 
                                                    <span class='glyphicon glyphicon-chevron-right'></span>
                                                </a>
                                                <a class='btn btn-primary' href='portfolio-item.php?id=".$row['id']."'>Go To Project 
                                                    <span class='glyphicon glyphicon-chevron-right'></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    ";
                }


            }elseif ($page == 4) {
                /**
                 * it will print 4 project in one line
                 */
                while ($row = $result->fetch_assoc()){

                    $projectCard .= "
                                    <div class='col-lg-3 col-md-4 col-sm-6 portfolio-item'>
                                        <div class='card h-100'>
                                            <a href='portfolio-item.php?id=".$row['id']."'>
                                            <img class='card-img-top' src='http://placehold.it/700x400' alt=''></a>
                                            <div class='card-body'>
                                                <h4 class='card-title'>
                                                    <a href='#'> " . $row["name"] . " </a>
                                                </h4>
                                                <p class='card-text'>" . $row["description"] . " .</p>
                                                <a class='btn btn-primary' href='portfolio-item.php?id=".$row['id']."'>More Info 
                                                    <span class='glyphicon glyphicon-chevron-right'></span>
                                                </a>
                                                <a class='btn btn-primary' href='portfolio-item.php?id=".$row['id']."'>Go To Project 
                                                    <span class='glyphicon glyphicon-chevron-right'></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    ";
                }
            }else {
                echo "<script>window.location = '404.php'</script>";
            }
            
            



        } else {
            $projectCard = " no project ";
        };


        
        return $projectCard;
    }


    /**
     * this function will disblay the data of the selectid project 
     */
    public function showOneProject($id){
        
        global $conn;
        $sql = "SELECT * FROM projects_name WHERE id= '$id'";
        $result = $conn->query($sql);

        if ( $result->num_rows == 1 ) {
            $row = $result -> fetch_assoc();

            $projectName = $row["name"];
            $projectDescription = $row["description"];
            $projectLink = $row["link"];

            
        } else {
            echo "
            <script>
                window.location = '404.php'
            </script>";
        }
        
        return $row;

    }


    /**
     * 
     */
    public function nextProjects($id){
        global $conn;
        $sql = "SELECT * FROM projects_name LIMIT $id,4 ";
        $result = $conn->query($sql);

        if( $result->num_rows > 0 ){
            $imgs = "";
            // $row = $result->fetch_assoc();

            while ($row = $result->fetch_assoc() ) {
                if($row["id"]==$id){
                    $imgs = "";
                }else {
                    $imgs .= "
                        <div class='col-md-3 col-sm-6 mb-4'>
                            <a href='portfolio-item.php?id=".$row["id"]."'>
                                <img class='img-fluid' src='http://placehold.it/500x300' alt=''>
                                <h2> " . $row["name"] . " </h2>
                            </a>
                        </div>
                        ";
                }
                
            }

        }else{
            echo "<h1> This Is The Last Project </h1>";
        }

        echo $imgs;
    }


}













?>